package example.com.interappsender;
import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText name, password;
    private Context context;
    public MainActivity(){
        this.context = MainActivity.this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name = findViewById(R.id.name);
        password = findViewById(R.id.password);

        MainActivity.this.registerReceiver(  new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle bundle = intent.getExtras();
                String name = bundle.getString("userName");
                String password = bundle.getString("password");
                Intent show = new Intent(MainActivity.this, example.com.interappsender.ShowResultOfReceiver.class);
                show.putExtra("name",name);
                show.putExtra("password",password);
                startActivity(show);
            }
        },new IntentFilter("com.arabin.CUSTOM_ACTION"));

    }

    public void sendData(View view) {
        Bundle bundle = new Bundle();
        bundle.putString("userName",name.getText().toString());
        bundle.putString("password",password.getText().toString());
        Intent intent = new Intent("com.arabin.CUSTOM_ACTION");
        intent.putExtras(bundle);
        //intent.putExtra("password",password.toString());
        sendBroadcast(intent);

    }
}
