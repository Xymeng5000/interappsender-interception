package example.com.interappsender;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ShowResultOfReceiver extends AppCompatActivity {


    private TextView name,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_result_of_receiver);
        name = findViewById(R.id.userName);
        password = findViewById(R.id.pass);

        Bundle data = getIntent().getExtras();
        String userName = data.getString("name");
        String pass = data.getString("password");

        name.setText(userName);
        password.setText(pass);
    }
}
